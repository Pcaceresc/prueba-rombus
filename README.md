Instructivo Prueba | Front-End Developer | Rombus Global


Hola, bienvenido/a a la fase 2.

Damos por hecho que ya has leído y comprendido los requerimientos y propuesta asociativa de Rombus. Si quieres volver a repasar el documento, ingresa en este enlace: https://drive.google.com/file/d/1oSFM4pQCHPAcJ2p94gQeWWY9zmriFp4t/view?usp=sharing

Esta prueba consiste en maquetar las vistas del proyecto que recibirás por invitación a Zeplin. 
Las maquetas deben ser realizadas utilizando HTML5, CSS3 y Boostrap. En cuanto a las funcionalidades se puede utilizar Javascript para las mismas, el sitio actual utiliza Angular como framework pero el uso del mismo no es obligatorio en esta entrega.

· Acceso Zeplin Web: https://zpl.io/aMAppLo
· Acceso Zeplin App: zpl://project?pid=5ef34c684718c280bf3e3063

Allí encontrarás un “Wizard" de carga de proyectos.

· Consiste en una guía de 4 pasos y una 5ta. ficha resúmen que las empresas deben completar para publicar sus proyectos de trabajo en la plataforma.

Para tener una idea, te compartimos esta presentación donde se puede visualizar -y navegar parcialmente- el prototipo de diseño en la versión Desktop: 
https://xd.adobe.com/view/08cfde03-aa4c-4ce2-49b3-a8b19880b88d-7efe/?fullscreen&hints=off

Como verás, son una serie de varias pantallas, van en versión Desktop y Mobile, no es requisito que maquetes todas, pero si las que alcances a completar en el tiempo disponible para realizar la prueba.


Tecnologías a aplicar:
HTML5 + CCS3 + Bootstrap

Otros requisitos del perfil:
· GIT
· Angular deseable, no excluyente
· Experiencia en scrum/agile y proyectos con equipos distribuidos





Los criterios de evaluación son, por orden de prioridad:

Prioridad 1: la calidad del trabajo (fidelidad del diseño, orden, prolijidad y detallismo).
Prioridad 2: el grado de avance (cantidad de pantallas completas que se alcanzan a incluir en la entrega, a término).
Prioridad 3: la fecha de entrega (en el caso de completar todo antes de la fecha final).


Plazo y fecha de entrega: desde hoy y hasta el viernes 3 de julio a las 23:59 hs.



¡Te deseamos el mayor de los éxitos!
