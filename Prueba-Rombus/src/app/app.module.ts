import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './modules/component/app.component';
import { Step1Component } from './modules/step1/step1.component';
import { Step2Component } from './modules/step2/step2.component';
import { Step3Component } from './modules/step3/step3.component';
import { Step4Component } from './modules/step4/step4.component';
import { Step5Component } from './modules/step5/step5.component';
import { HeaderComponent} from './modules/shared/header/header.component';
import { FooterComponent} from './modules/shared/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    Step5Component,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
